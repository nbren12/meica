#!/usr/bin/env python


from pylab import *
from scipy.fftpack import dct,idct
from scipy.linalg import toeplitz
import nibabel as nib
from tedana import fmask, unmask,cat2echos,niwrite
from argparse import ArgumentParser
import sys,os

def threshAtNNZ(c,nnz):
    inds = argsort(c**2)
    out = c.copy()
    out[inds[:-nnz]] = 0
    return out


class TsT2:
    """A Class for doing stuff"""


    def __init__(self,data,tes):
        """
        data has shape (ne,nt)
        """
        self.ne,self.nt = data.shape
        self.data = data
        self.mu = data.mean(axis=-1)
        self.tes = tes
        # self.DCT = dct(eye(self.nt),norm='ortho')
        self.fitmod()


    def fitmod(self):
        edat = self.data.copy()
        mu   = self.mu
        tes= self.tes

        edat = edat-mu[:,newaxis]
        self.dm = edat.copy().ravel()
        self.edat = edat

        X = zeros((self.ne,2))

        X[:,0] = 1
        X[:,1] = -tes # remember the tes.mean
        W = X.copy()
        X = dot(diag(mu),X)
        self.X = X

        Xrhop = X[:,0]-dot(X[:,0],X[:,1])*X[:,1]/(X[:,1]**2).sum()


        delta = dot(pinv(X),edat)
        self.ds0 = delta[0,:]
        self.spinw = kron(Xrhop,ones(self.nt))
        self.Xrhop = Xrhop

        self.dr2s = delta[1,:]
        
    

        ehats0 = outer(X[:,0],self.ds0)
        ehatr2 = outer(X[:,1],self.dr2s)

        self.fitt = dot(X,delta)

        # self.fitt = ehats0+ehatr2
        # self.resid  = edat-ehats0-ehatr2
        self.resid = edat - self.fitt
        self.dn = (self.resid+ehatr2).mean(axis=0)


        
        # Calculate Standard error of the dr2s time series

        SE = (self.resid**2).sum()/(self.nt*(self.ne-2))*inv(dot(self.X.T,self.X))
        SE = sqrt(SE[1,1])
        m = (self.dr2s<1.5*SE) & (self.dr2s > -1.5*SE)
        

        # this multiplication makes instacorr calcluate the coefficents correctly
        tmp = (1-m)*self.nt/float(max((1-m).sum(),1))
        self.filt = tmp
        self.filt = ones(self.nt)
        self.filt[m] = 0


        m = abs(self.dr2s)> SE 

        self.filt = ones(self.nt)
        self.filt[m] = 0


    def numart(self):

        edat = self.data
        LHS = trapz(edat,x=self.tes,axis=0)
        self.t2s = LHS/(edat[0,:]-edat[-1,:])
        r = exp(-self.tes[:,newaxis]/self.t2s[newaxis,:])

        num = (r*edat).sum(axis=0)
        denom = (r**2).sum(axis=0)

        self.s0_numart = num/denom
        self.resid_numart = (edat-self.s0_numart[newaxis,:]*r).ravel()

    def fitmod_loglin(self):
        edat = self.data
        ne,nt = edat.shape

        edat = edat.ravel()

        X = []
        # form constant accross echo
        for ee in range(ne):
            tmp = zeros((nt*ne,1))
            tmp[ee*nt:(ee+1)*nt,0] = ones(nt)
            X.append(tmp)

        # form spin density columns
        X.append(vstack([eye(nt)]*ne))

        # form t2s columns
        X.append(vstack([eye(nt)*tes[ee] for ee in range(ne)]))
        k=2
        X = hstack(X)
        U,S,V = svd(X,full_matrices=False)
        U = U[:,:-k]
        S = S[:-k]
        V = V[:-k,:]

        pX = dot(V.T,dot(diag(1/S),U.T))


        betas = dot(pX,self.data.ravel())
        self.r2s_log = betas[-nt:]
        self.s0_log  = exp(betas[-2*nt:-nt])

    def fitmod_DCT(self,a=60):
        """
        This method is not that good
        """

        # Bring intance variables into local namespace
        edat = self.data
        tes = self.tes
        ne = self.ne
        nt = self.nt

        nnz = lambda c : (c!=0).sum()
        rel_err = lambda a,b : 100*norm(a-b)/norm(a)
        nnz = lambda c : (hstack(c)!=0).sum()
        threshcols = lambda c,nnz : (threshAtNNZ(c,nnz)!=0).nonzero()[0]

        

        # Compress this stuff
        cdct = map(lambda x : dct(x,norm='ortho'),edat)
        cols = unique(hstack(map(lambda x : threshcols(x,a),cdct)))


        # Form the Multiecho Weights
        mu = self.mu
        w1 = tes*mu
        w2 = mu

        # Form the Multiecho DCT matrix
        DCT = self.DCT[cols,:]
        nr = cols.shape[0]
        G= []
        B =[]
        for rr in range(nr):
            tmp = DCT[rr,:]
            
            tmpr2 = hstack([tmp*w1[ee] for ee in range(ne)])
            tmps0 = hstack([tmp*w2[ee] for ee in range(ne)])

            G.append(tmpr2)
            B.append(tmps0)

        G = vstack(G)
        B = vstack(B)

        X = vstack([G,B]).T


        # Form the tcat and decompose
        tcat = edat.ravel()
        coef = lstsq(X,tcat)[0]

        # Recompose
        nr = cols.shape[0]
        fitted  = dot(X,coef)

        scoefs = [coef[:nr],coef[nr:2*nr]]

        dr2c = coef[:nr]
        


        self.dr2_DCT = dot(DCT.T,coef[:nr])
        self.ds0_DCT = dot(DCT.T,coef[nr:2*nr])
    
    # def fitmod_tik(self,lam=30000):
    #     nt = self.nt
    #     ne = self.ne
    #     tcat = self.data.ravel()
    #     GAM = hstack([eye(nt)]*2)*lam

    #     w1 = 
 
    #     colwg = zeros(ne*nt)
    #     colwb = zeros(ne*nt)

    #     for ee in range(ne):
    #         colwg[ee*nt] = w1[ee] 
    #         colwb[ee*nt] = w2[ee]

    #     Wg = toeplitz(colwg)[:,:nt]
    #     Wb = toeplitz(colwb)[:,:nt]

    #     W = hstack([Wg,Wb])
         
    #     A = dot(W.T,W)+dot(GAM.T,GAM)
    #     tmp = dot(W.T,tcat)
    #     beta = solve(A,tmp)

    #     self.dr2_tik,self.ds0_tik = split(beta,2)


    def summary_plot(self):

        plot((self.data-self.mu[:,newaxis]).T)
        show()

    def set_data(self,data):
        self.data = data
        self.mu = self.data.mean(axis=-1)
        self.fitmod()
        self.numart()




if __name__=='__main__':

    SS = lambda data : (data**2).sum()

    parser = ArgumentParser()
    parser.add_argument('-m',dest='mask',help="Mask",required=True,default=None)
    parser.add_argument('-e',dest='echos',help="Echo times",required=True,default=None)
    parser.add_argument('-d',dest='data',help="Data",required=True,default=None)


    args = parser.parse_args()

    tes  = fromstring(args.echos,sep=',',dtype=float)

    ne   = tes.shape[0]

    if not os.path.exists(args.mask) or not os.path.exists(args.data):
        print "No Files found at paths...Exiting"
        sys.exit(1)

    maskim = nib.load(args.mask)
    dataim = nib.load(args.data)


    aff    = dataim.get_affine()
    dat    = cat2echos(dataim.get_data(),ne)
    mask   = (maskim.get_data()!=0)

    nm     = mask.sum()
    nx,ny,nz,ne,nt = dat.shape

    # shape of mdat is (nm,ne,nt)
    mdat   = fmask(dat,mask)
    mu     = mdat.mean(axis=-1)

    # shape is now (nm,nt,ne)
    # mdat   = mdat.transpose((0,2,1))

    # shape is now (nm*nt,ne)
    # mdat   = reshape(mdat,(nm*nt,ne))

    ### Code to do fit

      
    X = zeros((3,2))
    Xlin = zeros((3,2))
    Xlin[:,0] = ones(3)
    Xlin[:,1] = -tes

    Xorth = zeros((3,2))
    #Xorth[:,0] = ones(3)
    #Xorth[:,1] = -tes +tes.mean()
    #X = Xorth.copy()
    w1 = array([1.0,1.0,1.0])
    w1 /= norm(w1)
    w2 = tes/norm(tes) # should be minus, but then coeffs will be minus
    w1_orth = w1 - dot(w1,w2)*w2
    w1_orth = w1_orth/norm(w1_orth)

    # Xorth[:,0] = w1_orth
    # Xorth[:,1] = w2

    dr2 = zeros((nm,nt))
    ds0 = zeros((nm,nt))
    ws0 = zeros((nm,ne*nt))
    ws0p = zeros((nm,ne*nt))


    dm =zeros((nm,nt*ne))
    dn = zeros((nm,nt))
    resid = zeros((nm,nt))
    sig = zeros((nm,nt))

    filt = zeros((nm,nt))

    o = TsT2(mdat[0,:,:],tes)
    
    figure()
    for i in range(nm):
        if i%100==1:
            print "%2.2f"%(float(i)*100/nm)

        o.set_data(mdat[i,:,:])        
        dr2[i,:] = o.dr2s
        ds0[i,:] = o.ds0
        resid[i,:] = o.resid[1,:]
        dn[i,:] = o.dn
        # dr2_DCT[i,:] = o.dr2_DCT
        # ds0_DCT[i,:] = o.ds0_DCT
        # dr2_tik[i,:] = o.dr2_tik
        # ds0_tik[i,:] = o.ds0_tik
        sig[i,:] = (o.resid**2).sum(axis=0)
        filt[i,:] = o.filt
        ws0p[i,:] = kron(o.Xrhop,ones(nt))
        ws0[i,:]= kron(o.X[:,0],ones(nt))
        dm[i,:] = o.dm



        
        # # Linearized Fit
        # edat = mdat[i,:,:]
        # mu   = edat.mean(axis=-1)
        # edat = edat-mu[:,newaxis]
        # X[:,0] = mu
        # X[:,1] = -mu*tes/tes.mean() # remember the tes.mean
        
        # delta = dot(pinv(X),edat)
        
        # # Spin-only and R2-only models
        # delta_s0 = zeros(nt)
        # delta_r2 = zeros(nt)

        # for ee in range (ne):
        #     delta_s0 += X[ee,0]*edat[ee,:]
        #     delta_r2 += X[ee,1]*edat[ee,:]

        # delta_s0 = delta_s0/(X[:,0]**2).sum()
        # delta_r2 = delta_r2/(X[:,1]**2).sum()


        # fitts_f  = dot(X,delta)
        # fitts_s0 = outer(X[:,0],delta_s0)
        # fitts_r2 = outer(X[:,1],delta_r2)

        # # This Rsq definition is from Saad, et al. 2010
        # Rsq_s0[i] = 1.0-SS(edat-fitts_f)/SS(edat-fitts_r2)
        # Rsq_r2[i] = 1.0-SS(edat-fitts_f)/SS(edat-fitts_s0)

        # mus[i,:] = mu
        # ds0[i,:] = delta[0,:]
        # dr2[i,:] = delta[1,:]
        
        # ehats0 = outer(X[:,0],delta[0,:])
        # ehatr2 = outer(X[:,1],delta[1,:])
        # residi  = edat-ehats0-ehatr2

        # resids[i,:] =  reshape(residi,(nt*ne))
        # st()
        # dn[i,:] = reshape(edat-ehats0,(nt*ne))
            

        # errn[i]  = SS(edat-edat.mean())
        # errs0[i] = 1-SS(edat - fitts_s0)/errn[i]
        # errr2[i] = 1-SS(edat - fitts_r2)/errn[i]
        # errf[i]  = 1-SS(edat-fitts_f).sum()/errn[i]
        
        # Log Linear Fit
        #edat = mdat[i,:,:]
        #ldat = log(edat)
        #betalog = dot(pinv(Xlin),ldat)
        #r2log[i,:] = betalog[1,:]
        #s0log[i,:] = exp(betalog[0,:])
        
        # Linear Fit Orthogonal
        #edat = mdat[i,:,:]
        #edat = (edat-mu[:,newaxis])/mu[:,newaxis]
        #beta = dot(pinv(Xorth),edat)
        #dr_orth[i,:] = beta[1,:]
        #ds0_orth[i,:] = beta[0,:]
        
        
       
if True:

    out = unmask(ds0,mask)
    niwrite(out,aff,'ds0.nii')

    out = unmask(dr2,mask)
    niwrite(out,aff,'dr2.nii')

    out = unmask(dm,mask)
    niwrite(out,aff,'tcat_dm.nii')

    out = unmask(sig,mask)
    niwrite(out,aff,'sig.nii')

    out = unmask(resid,mask)
    niwrite(out,aff,'resid.nii')

    out = unmask(ws0p,mask)
    niwrite(out,aff,'ws0p.nii')

    out = unmask(ws0,mask)
    niwrite(out,aff,'ws0.nii')


