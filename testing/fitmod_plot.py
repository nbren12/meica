from fitmod import *

close('all')

tes = array([15.0,39.0,63.0])


# e1 = genfromtxt('e1_mot.txt');
# e2 = genfromtxt('e2_mot.txt');
# e3 = genfromtxt('e3_mot.txt');


e1 = genfromtxt('e1_ha.txt') ;
e2 = genfromtxt('e2_ha.txt') ;
e3 = genfromtxt('e3_ha.txt') ;

# e1 = genfromtxt('e1_wm.txt');
# e2 = genfromtxt('e2_wm.txt');
# e3 = genfromtxt('e3_wm.txt');

# e1 = genfromtxt('e1_wm2.txt');
# e2 = genfromtxt('e2_wm2.txt');
# e3 = genfromtxt('e3_wm2.txt');

# e1 = genfromtxt('e1_lh.txt');
# e2 = genfromtxt('e2_lh.txt');
# e3 = genfromtxt('e3_lh.txt');

nt = e1.shape[0]
ne = tes.shape[0]

o= TsT2(vstack([e1, e2, e3]),tes)

demean = lambda x : x - x.mean(axis=-1)[:,newaxis]
psc = lambda x : (x - x.mean(axis=-1)[:,newaxis])/x.mean(axis=-1)[:,newaxis]



plot(100*psc(o.data).T)
ylabel('X Signal Change')
legend(['15ms','39ms','63ms'])
savefig('three-echo.png')

figure()

plot(-100*o.dr2s*tes[2])
plot(100*o.ds0)

legend(['BOLD','Spin-density'])
ylabel('Percent Signal Change')
# title('Echo 2 Fitted Values')
savefig('fitted-vals.png')

figure()


# Using unbiased estimate for S^2, instead of mle sigma_hat^2
SE = (o.resid**2).sum()/(nt*ne-nt*2)*inv(dot(o.X.T,o.X))
SE = sqrt(SE[1,1])

plot(o.dr2s)
plot(o.dr2s-SE,'r--')
plot(o.dr2s+SE,'r--')

m = (o.dr2s<1.5*SE) & (o.dr2s > -1.5*SE)
tmp = o.dr2s.copy()
tmp[m] = 0


show()


