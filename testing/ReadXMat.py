import numpy as np
import statsmodels.api as sm
import sys,re
from ipdb import set_trace as st


def interspace(data,ee,ne=3):
    nt =data.shape[0]
    out = np.zeros(nt*ne)
    out[ee::ne] = data

    return out
def shift(data,ee,ne=3):
    nt =data.shape[0]
    out = np.zeros(nt*ne)
    out[ee*nt:(ee+1)*nt] = data

    return out
def deinterspace(data,ne=3):
    nt = data.shape[0]/ne
    echos = []
    for ee in range(ne):
        echos.append(data[ee::ne])
    out = np.hstack(echos)
    return out


class XMat(object):
    
    def __init__(self,X,regdict):
        self.X = X
        self.regdict = regdict
        self.parse_info()
        self.results= None
        

    def parse_info(self):
        self.nt , self.nr = self.X.shape
    

    def regress(self,data,rho=6,maxiter=2):
        self.data = data
        model = sm.GLSAR(data,self.X,rho=rho)
        res   = model.iterative_fit(maxiter=maxiter)
        
        self.results=res

    def ols(self,data):
        self.data = data
        self.model = sm.OLS(data,self.X)
        res = self.model.fit()
        self.results=res
    
    def gls(self,data,sig):
        self.data = data
        self.sigma= sig
        
        self.model = sm.GLS(data,self.X,sigma=sig)
        res = self.model.fit()
        self.results=res

    # TODO : Not sure this is necesary. Delete - NB 5/30/2012
    def set_sig(self,sig):
        self.sig = sig 

    def do_ftests(self):
        Fdict = {}
        if self.results is None:
            print "Must do self.regress()"
            sys.exit(0)
        for key in self.regdict:
            if key !='base':
                Fdict[key] = self.results.f_test(self.get_ftest(key))

        return Fdict

    def get_ftest(self,key):
        inds = self.regdict[key]
        data = []
        if inds.__class__ == int: inds=[inds]
        for ii in inds:
            tmpi = np.zeros(self.nr)
            tmpi[ii] = 1
            data.append(tmpi)
        return np.array(data)

    def summary(self):
        return self.results.summary()

    def get_errts(self): return self.results.resid
    def get_fitts(self): return self.results.fittedvalues
    def get_input(self): return self.data
    def get_werrts(self): return self.result.wresid

class CatXMat(XMat):
    def __init__(self,X,regdict,ne=3):
        super(CatXMat,self).__init__(X,regdict)
        self.ne= ne
        self.nt =self.nt/self.ne
        


    def get_errts(self,sep_echos=True): 

        if sep_echos:
            return np.reshape(self.results.resid,(self.ne,self.nt)).T
        else:
            return super(CatXMat,self).get_errts()

    def get_fitts(self,sep_echos=True): 

        if sep_echos:
            return np.reshape(self.results.fittedvalues,(self.ne,self.nt)).T
        else:
            return super(CatXMat,self).get_fitts()

    def get_input(self,sep_echos=True): 

        if sep_echos:
            return np.reshape(self.data,(self.ne,self.nt)).T
        else:
            return super(CatXMat,self).get_input()

class InterleavedXMat(XMat):

    def get_errts(self): return deinterspace(self.results.resid,ne=3)
    def get_fitts(self): return deinterspace(self.results.fittedvalues,ne=3)
    def get_input(self): return deinterspace(self.data,ne=3)
    def get_werrts(self): return deinterspace(self.results.wresid,ne=3)

class AFNIXMat(XMat):
    
    def __init__(self,ne,fname=None):
        self.ne = ne
        self.info = {}
        self.X = None
        if fname:
            self.parse_file(fname)
            self.parse_info()
        
        
    def parse_file(self,fname):
        datastr = ''
        self.filename =fname
        with open(self.filename,'r') as f:
            for line in f:
                if re.match(r'\# </',line):
                    break
                elif not re.match(r'\#',line):
                    datastr+=line
                elif not re.search(r'[<,>]',line):
                    mat = re.search(r'# \(.*\) = \(.*\) ',line)
                    key,val = re.search(r'\#\s*(.*) = "(.*)"\s',line).groups()
                    self.info[key] = val
        

        data = []
        for line in datastr.strip().split('\n'):
            data.append(np.fromstring(line,sep=' '))

        self.X = np.array(data)
        nr = self.X.shape[-1]
        
        # Parsing Header
        self.Nstim = int(self.info['Nstim'])
        bfun = lambda x : re.match("BasisName",x) 
        basis_keys = filter(bfun,self.info)

        regdict = {}
        baseline = range(nr)
        for kk in basis_keys:
            key,num = kk.split('_')
            name = self.info[kk]
            col  = int(self.info["BasisColumns_"+num][0])
            regdict[name] = col
            baseline.remove(col)

        # TODO : This 'base' string will break if one of the regressors is named base
        regdict['base'] = baseline
        self.regdict = regdict
    
    def form_Xcat(self):

        ne =self.ne
        nt = self.nt
        nr = self.nr
        X = self.X

        X_il   = np.zeros((ne*nt,ne*nr))

        for ee in range(ne):
            for rr in range(nr):
                X_il[:,ee+rr*ne] = shift(X[:,rr],ee,ne=ne)


        regdict = {}
        baseline = range(nr*ne)
        for reg in self.regdict:
            val = self.regdict[reg]

            if reg != 'base':
                regdict[reg] = range(ne*val,ne*val+ne)

                for kk in regdict[reg]: baseline.remove(kk)

        regdict['base'] = baseline

        return CatXMat(X_il,regdict,ne=ne)
    
    def form_IXcat(self,tes,mu):
        """
        Fits the model dS = mu*(pho'/pho - tes *r2')
        """
        nr = 2*self.Nstim + (self.nr -self.Nstim)*self.ne
        bbase = 2*self.Nstim
        nt = self.nt*self.ne
        
        regdict = {}
        X = np.zeros((nt,nr))


        w_s0 = mu
        w_r2 = tes*mu # should be minus, but then coeffs will be minus

        w_s0 /= np.linalg.norm(w_s0)
        w_r2 /= np.linalg.norm(w_r2)

        w_orth = w_s0 - np.dot(w_s0,w_r2)*w_r2
        w_orth /= np.linalg.norm(w_orth)

        count = 0

        # Weight the regressors according to the TE model
        for reg in self.regdict:
            col = self.regdict[reg]
            col_list = []

            if reg == 'base':
                for rr in col:
                    reg_ts = self.X[:,rr]
                    for ee in range(self.ne):
                        X[:,bbase] = shift(reg_ts,ee,ne=self.ne)

                        col_list.append(bbase)
                        bbase+=1
                    regdict[reg] = col_list

            else:
                reg_ts = self.X[:,col]
                regdict[reg] = count
                for ee in range(self.ne):
                    
                    tmp = shift(reg_ts,ee,ne=self.ne)*w_r2[ee]
                    X[:,count] += tmp

                    tmp = shift(reg_ts,ee,ne=self.ne)*w_orth[ee]
                    X[:,count+1] += tmp

                count +=2
                

        return CatXMat(X,regdict)

    def form_IXfull(self):

        ne =self.ne
        nt = self.nt
        nr = self.nr
        X = self.X

        X_il   = np.zeros((ne*nt,ne*nr))

        for ee in range(ne):
            for rr in range(nr):
                X_il[:,ee+rr*ne] = shift(X[:,rr],ee,ne=ne)

        regdict = {}
        baseline = range(nr*ne)
        for reg in self.regdict:
            val = self.regdict[reg]

            if reg != 'base':
                regdict[reg] = range(ne*val,ne*val+ne)

                for kk in regdict[reg]: baseline.remove(kk)

        regdict['base'] = baseline

        return InterleavedXMat(X_il,regdict)

if __name__ == '__main__':
    import sys

    tes = np.array([1,2,3])
    mu  = np.ones(3)
    o = AFNIXMat(3,fname=sys.argv[1])
    r = o.form_Xcat(tes,mu)

        


