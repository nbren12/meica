from pylab import *
import nibabel as nib
import tedana as ted

def invWbyX(X,u,v,d,e):
	"""
	input :

	size(X) = n,3,t  		this is the data
	size(u) = size(v) = 3  	these are the TEs or a constant
	size(d) = size(e) = n  	these are the voxelwise weights (i.e. temporal mean)

	output : 

	shape(G) = n,t 	 	this is the BOLD-like portion of the data
	shape(B) = n,t 		this is the spin-history like portion of the data
	"""
	nn,q,nt = X.shape

	A = (u**2).sum()*d**2
	D = (v**2).sum()*e**2
	C = B = dot(u,v)*e*d

	P11 = 1/(A-B*C/D)
	P22 = 1/(D-C*B/A)

	P21 = -P22*C/A
	P12 = -P11*B/D

	INV = []
	for i in range(3):
		INV.append( [P11*u[i]*d + P12*v[i]*e,P21*u[i]*d+P22*v[i]*e] )


	G = zeros((nn,nt))
	B = zeros((nn,nt))
	for te in range(3):
		G += INV[te][0][:,newaxis]*X[:,te,:]
		B += INV[te][1][:,newaxis]*X[:,te,:]

	return  G,B




