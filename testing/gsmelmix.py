import os,sys
import numpy as np

mixname = 'melodic_mix'
ordname = 'order.txt'
outname = 'regsoni'


mixmat = np.genfromtxt(mixname)
korder = np.genfromtxt(ordname,dtype=np.int)-1


#rearrange columns
mixmat = mixmat[:,korder]

#gramschmidt starting from the left
q,r = np.linalg.qr(mixmat)

#regressors of no interest
badies = q[:,30:]

np.savetxt(outname,badies)
