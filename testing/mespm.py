#!/usr/bin/env python

from pylab import *
import sys,os
from ipdb import set_trace as st
sl = []

tes = array([11.0,23.04,35.08])

regnames = sys.argv[1:]

regs= [genfromtxt(name) for name in regnames] 
regs =[reg for reg in regs]

w1 = array([1.0,1.0,1.0])
w1 /= norm(w1)
w2 = tes/norm(tes) # should be minus, but then coeffs will be minus
w1_orth = w1 - dot(w1,w2)*w2
w1_orth = w1_orth/norm(w1_orth)

nc = len(regs)
nt = regs[0].shape[0]
ne = tes.shape[0]
out1 = zeros(ne*nt)
out2 = zeros(ne*nt)
out3 = zeros(ne*nt)

sl.append('-num_stimts %d'%(2*nc))

for j in range(nc):
    for i in range(ne):
        ip1 = i+1
        out1[i*nt:ip1*nt] = w1[i]*regs[j]
        out2[i*nt:ip1*nt] = w2[i]*regs[j]
        out3[i*nt:ip1*nt] = w1_orth[i]*regs[j]
        
    name, ext = os.path.splitext(regnames[j])
    
    gname = '%s_bold.1D'%name
    savetxt(gname,out2)
    sl.append('-stim_file %d %s -stim_label %d %s_bold'%(2*j+1,gname,2*j+1,name))
    
    bname = '%s_mot.1D'%name
    savetxt(bname, out1)
    #sl.append('-stim_file %d %s -stim_label %d %s_mot'%(2*j+2,bname,3*j+2,name))
    
    bname = '%s_orthmot.1D'%name
    savetxt(bname, out3)
    sl.append('-stim_file %d %s -stim_label %d %s_orthmot'%(2*j+2,bname,2*j+2,name))
        
print ' \\\n'.join(sl)        


