#!/bin/bash

FWHM=$1

if [ ! -f ds0_tc.nii.gz ]
then

    3dTcat -prefix ds0_tc.nii.gz ds0.nii ds0.nii ds0.nii

fi

if [ ! -f tcat_dn_${FWHM}mm_in.nii.gz ]
then

3dBlurInMask -fwhm ${FWHM}mm -prefix ds0_tc${FWHM}mm.nii.gz -mask ../eBvrmask.nii.gz ds0_tc.nii.gz -overwrite
3dcalc -a ds0_tc${FWHM}mm.nii.gz  -b ws0p.nii -expr 'a*b' -prefix ds0_tcw${FWHM}mm.nii.gz -overwrite

3dTfitter -RHS tcat_dm.nii -LHS ds0_tcw${FWHM}mm.nii.gz -prefix tfit_${FWHM}mm.nii -overwrite

3dcalc -a tfit_${FWHM}mm.nii  -b ws0.nii -c  ds0_tc${FWHM}mm.nii.gz -expr 'a*b*c' -prefix bad_${FWHM}mm.nii.gz -overwrite

3dcalc -a bad_${FWHM}mm.nii.gz -b tcat_dm.nii -expr 'b-a' -prefix tcat_dn_${FWHM}mm.nii.gz -overwrite

3dBlurInMask -mask ../eBvrmask.nii.gz  -fwhm 5mm -prefix tcat_dn_${FWHM}mm_in.nii.gz tcat_dn_${FWHM}mm.nii.gz -overwrite

fi

3dTcat -prefix e2_dn_${FWHM}mm_in.nii.gz tcat_dn_${FWHM}mm_in.nii.gz[240..479]
