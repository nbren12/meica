#!/usr/bin/env python


from pylab import *
import nibabel as nib
from tedana import fmask, unmask,cat2echos,niwrite
from argparse import ArgumentParser
from ipdb import set_trace as st
import sys,os

parser = ArgumentParser()
parser.add_argument('-m',dest='mask',help="Mask",required=True,default=None)
parser.add_argument('-d',dest='data',help="Data",required=True,default=None)

args = parser.parse_args()


if not os.path.exists(args.mask) or not os.path.exists(args.data):
    print "No Files found at paths...Exiting"
    sys.exit(1)

maskim = nib.load(args.mask)
dataim = nib.load(args.data)


aff    = dataim.get_affine()
dat    = dataim.get_data()
mask   = (maskim.get_data()!=0)

nm     = mask.sum()
nx,ny,nz,nt = dat.shape

# shape of mdat is (nm,ne,nt)
mdat   = fmask(dat,mask)
mu     = mdat.mean(axis=-1)

Xcorr  = corrcoef(mdat.T)
U,S,V  = svd(Xcorr)

savetxt('sigma',cov(mdat.T))
savetxt('prewhiten',inv(cholesky(cov(mdat.T))))

x = linspace(0,1,nt)
plot(cumsum(S)/S.sum())
figure()
imshow(Xcorr,vmin=-1,vmax=1,interpolation='nearest')
colorbar()
show()




