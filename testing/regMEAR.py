from ReadXMat import *
import nibabel as nib
from tedana import *
from ipdb import set_trace as st



# Define Inputs
xmat_name = 'se.xmat.1D'
datasets = ['e1_sm.nii.gz','e2_sm.nii.gz','e3_sm.nii.gz']
mask = 'eBvrmask.nii.gz'
tes = np.array([15.0,39.0,63.0]); ne = tes.shape[0]

# Load Inputs
images = [nib.load(dd) for dd in datasets]
M = nib.load(mask).get_data()
X = AFNIXMat(ne,fname=xmat_name)
data = [fmask(im.get_data(),M) for im in images]

nm = M.sum()

# demean data
mean = lambda x:x.mean(axis=-1)[:,np.newaxis]
mu = map(mean,data)
data = [dd-mm for dd,mm in zip(data,mu)]

# interleave data
data_il = np.reshape(np.dstack(data),(nm,-1),order='C')

# write interleaved data to disk
out = unmask(data_il,M)
niwrite(out,images[0].get_affine(),'interleaved.nii.gz')


def deinterleave(data,ne=ne):
    nm,ntne= data.shape
    return reshape(reshape(data,(nm,ntne/ne,ne)),(nm,ntne),order='F')


# Get the XMat objects we want
Xfull = X.form_Xfull()

# preallocate outputs
Fval  = np.zeros((nm,X.Nstim))
werrts = np.zeros((nm,1000))
errts = np.zeros((nm,nt))

for i in range(nm):
    if i%100==0:
        print float(i)*100/nm
    

    Xfull.ols(data_il[i,:])



    # Fdict = Xfull.do_ftests()
    
    # for rr in range(X.nr):
    #     Fval[i,0] = Fdict['Right'].fvalue
    #     Fval[i,1] = Fdict['Left'].fvalue

    
    # errti = Xfull.get_werrts()
    # werrts[i,:errti.shape[0]] = errti 

    errts[i,:] = Xfull.get_errts()

sig = corrcoef(errts.T)

for i in range(nm):
    edat = data_il[i,:]

    Xfull.set_sig(sig*edat.std()**2)
    Xfull.gls()
    
   
    

# need to output:
# errts
# T-values
# F-value
# beta-values


out = unmask(Fval,M)
niwrite(out,images[0].get_affine(),'fvals.nii.gz')

out = unmask(werrts,M)
niwrite(out,images[0].get_affine(),'werrts.nii.gz')






