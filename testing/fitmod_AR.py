from fitmod import *
from pylab import *
from scipy.linalg import toeplitz
import statsmodels.tsa.api as tsa
from statsmodels.graphics.tsaplots import plotacf


class AR(object):
	def __init__(self,data,p=1):
		self.data=data
		self.p = p
		self.calc_AR(data)
	
	def calc_AR(self,data):
		
		self.Z = self.formZ(data)
		self.coefs= lstsq(self.Z,data)[0]

	
	def formZ(self,data):
		p =self.p
		nt = data.shape[0]
		T = toeplitz(data,r=zeros(nt))
		T =T[:,1:(p+1)]
		return T

	def filter(self,data):
		fitts = dot(self.formZ(data),self.coefs)
		return data-fitts


close('all')

tes = array([15.0,39.0,63.0])

e1 = genfromtxt('e1_r1.txt');
e2 = genfromtxt('e2_r1.txt');
e3 = genfromtxt('e3_r1.txt');


edat =vstack([e1,e2,e3])


nt = e1.shape[0]
ne = tes.shape[0]


ts = TsT2(edat,tes)


model = tsa.AR(ts.resid[0,:])

o = AR(ts.resid[0,:])
r2_filt= o.filter(ts.dr2s)

plotacf(tsa.acf(r2_filt,nlags=60))
title('r2 acf')


s0_filt= o.filter(ts.ds0)
plotacf(tsa.acf(s0_filt,nlags=60))
title('s0 acf')


err_filt= o.filter(ts.resid[0,:])
plotacf(tsa.acf(err_filt,nlags=60))
title('err acf')

plotacf(tsa.acf(o.data))

show()




