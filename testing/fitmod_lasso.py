from pylab import *
from fitmod import *
from scipy.signal import convolve
from sklearn.linear_model import Lasso

tes = array([15.0,39.0,63.0])

e1 = genfromtxt('e1_r1.txt');
e2 = genfromtxt('e2_r1.txt');
e3 = genfromtxt('e3_r1.txt');

baselines  =  genfromtxt('lasso_base.1D')

demean = lambda x : x-x.mean()
normalize   = lambda x : x/x.std()

HRF = array([0,24.4876,98.3811,33.2969,4.09246,0.288748])
edat = vstack([e1,e2,e3])
tcat = edat.ravel()

o = TsT2(edat,tes)

nt = e1.shape[0]
ne = 3

w1 = o.X[:,0] ; w1 = w1/w1.std() 
w2 = o.X[:,1] ; w2 = w2/w2.std()


# Form the overcomplete dictionaries for the lambdas
# spin density is baselines + impulses
# r2 is HRfs + impulses

conv = lambda X :  apply_along_axis(lambda x : convolve(x,HRF)[1:nt+1],1,X)



PHI_G = hstack([conv(eye(nt)) ])
PHI_B = hstack([eye(nt), baselines, conv(eye(nt)) ])

# Form the Design matrix
Xg = []
Xb = []
for ee in range(ne):
	Xg.append(PHI_G*w2[ee])
	Xb.append(PHI_B*w1[ee])


Xg = vstack(Xg)
ng = Xg.shape[1]

Xb = vstack(Xb)
nb = Xb.shape[1]

X = hstack([Xg,Xb])

clf = Lasso(alpha=.1)
clf.fit(X,tcat)

dr2c = clf.coef_[:ng]
ds0c = clf.coef_[ng:]

dr2  = dot(PHI_G,dr2c)	
ds0  = dot(PHI_B,ds0c)	


