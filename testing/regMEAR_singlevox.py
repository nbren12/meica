from pylab import  *
import statsmodels.api as sm
import statsmodels.tsa.api as tsa
from ReadXMat import *
from statsmodels.tsa.stattools import acf,pacf
from statsmodels.graphics.tsaplots import plotacf
from scipy.stats import f
    

def formZ(data,nlag=4):
	"""
	data.shape = (ne,nt)
	"""
	ne,nt = data.shape
	Z = [ones(nt-nlag)]
	Z = []
	for ll in range (1,nlag+1):
		Z.append(data[:,nlag-ll:-ll])
		
	return vstack(Z)


def filterX(data,B,P,ne=3,nlag=4):
	"""
	data.shape(ne,nt)
	"""
	ne,nt = data.shape
	
	col = data.copy()
	
	Y = data[:,nlags:]

	Z = formZ(col,nlag)
	white = Y - dot(B,Z)
	white = solve(P,white)

	return white.ravel()




close('all') 

tes = array([15.0,39.0,63.0])

e1 = genfromtxt('e1_r1.txt');
e2 = genfromtxt('e2_r1.txt');
e3 = genfromtxt('e3_r1.txt');

edat =vstack([e1,e2,e3])
edat -= edat.mean(axis=1)[:,newaxis]

nt = e1.shape[0]
ne = tes.shape[0]


y = vstack((e1,e2,e3))

mu = y.mean(axis=-1)
y -= mu[:,newaxis]

ysum = y.sum(axis=0)
y = np.reshape(y,(nt*ne),order='F')

savetxt('interleave_r1.txt',y)


# Load AFNI X Matrix
fname = '/misc/sfim/brenowitznd/mespm/dat/sub5/meica.tapping1e213/se.xmat.1D'
Xafni = AFNIXMat(ne,fname=fname)

Xmecat =Xafni.form_IXcat(tes,mu)
Xmecat.ols(edat.ravel())
print Xmecat.summary()

Xcat = Xafni.form_Xcat()
Xcat.ols(edat.ravel())
print Xcat.summary()

# mod = tsa.VAR(Xcat.get_errts())
# res = mod.fit(4)

errts = Xcat.get_errts().T

# Form the Y = BZ+U , from http://en.wikipedia.org/wiki/General_matrix_notation_of_a_VAR%28p%29
nlags  = 2

inc = []


ft = Xcat.get_ftest('Right')

for ITER in range(4):

	Y = errts[:,nlags:]
	Z = formZ(errts,nlag=nlags)

	M=lstsq(Z.T,Y.T)
	B = M[0].T
	fitts_VAR = dot(B,Z)
	errts_VAR = Y - fitts_VAR

	# this sigma is the biased mle
	sigma_u_mle = dot(errts_VAR,errts_VAR.T)/(nt-nlags)

	# this sigma is the unbiased estimate
	sigma_u    = dot(errts_VAR,errts_VAR.T)/(nt -nlags-nlags*ne-1)

	# prewhitening
	P = cholesky(sigma_u)

	# Prewhiten X mat
	X = Xcat.X
	X_new = []
	nr =X.shape[1]
	for rr in range(nr):
		col = reshape(X[:,rr],(ne,nt))
		X_new.append(filterX(col,B,P,ne=ne,nlag=nlags))

	X_white = vstack(X_new).T

	# Prewhiten Data
	edat_white = filterX(edat,B,P,ne=ne,nlag=nlags)
	
	# Fit Whitened Model
	mod_white = sm.OLS(edat_white,X_white)
	res_white = mod_white.fit()

	# Calculate the New Residuals
	beta_white = res_white.params
	fitted_white = reshape(dot(X,beta_white),(ne,nt))
	errts = edat-fitted_white

	# Print some results
	print res_white.tvalues[12:]
	inc.append(res_white.ssr)	


resid = np.reshape(res_white.resid,(ne,nt-nlags))
mod_VAR  = tsa.VAR(resid.T)
res_VAR  = mod_VAR.fit()
print res_VAR.summary()
print res_white.summary()
fME= res_white.f_test(ft)

ysum=edat.sum(axis=0)

Xafni.ols(ysum)
print Xafni.summary
fsum = Xafni.do_ftests()['Right']

print f.logpdf(fsum.fvalue,fsum.df_num,fsum.df_denom)
print f.logpdf(fME.fvalue,fME.df_num,fME.df_denom)


xx = linspace(0,300,1000)
fMElogcdf = f.logcdf(xx,fME.df_num,fME.df_denom)
fsumlogcdf=f.logcdf(xx,fsum.df_num,fsum.df_denom)

plot(xx,fMElogcdf)
plot(xx,fsumlogcdf)
xlim([0,300])
ylim()
show()

# errts = reshape(res_white.resid,(3,nt-nlags))
# mod = tsa.VAR(errts.T)
# res = mod.fit(4)


# Commented out 5/30/2012
#
# Form Special Regression Matrices
# Xfull = Xafni.form_Xfull()
# savetxt('xfull.1D',Xfull.X,delimiter='\t')
# Xtcat = Xafni.form_Xcat(tes,mu)

# print "Sum Echos"
# Xafni.regress(ysum,rho=6)
# # print Xafni.summary()
# print Xafni.do_ftests()
# figure()
# plot(Xafni.get_fitts())
# plot(Xafni.get_input())

# print "\n\n\n"
# print "ME ind regressors"
# Xfull.regress(y,maxiter=0)
# print Xfull.summary()
# print Xfull.do_ftests()
# figure()
# plot(Xfull.get_fitts())
# plot(Xfull.get_input())

# title('Xfull maxiter = 0')
# savefig('/misc/sfim/brenowitznd/05-24-2012-Xfull-iter-0.png')
# show()
# Xfull.regress(y,maxiter=10,rho=4)
# print Xfull.summary()
# print Xfull.do_ftests()
# figure()
# plot(Xfull.get_fitts())
# plot(Xfull.get_input())

# title('Xfull maxiter = 5')
# savefig('/misc/sfim/brenowitznd/05-24-2012-Xfull-iter-5.png')
# show()





# print "\n\n\n"
# print "ME tcat regressors"
# Xtcat.regress(y)
# # print Xtcat.summary()
# print Xtcat.do_ftests()
# figure()
# plot(Xtcat.get_fitts())
# plot(Xtcat.get_input())

# show()

