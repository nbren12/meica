from pylab import *
from optparse import OptionParser
import tedana as ted
from tedana import fmask,unmask
from ipdb import set_trace as st
import nibabel as nib
import os

parser=OptionParser()
parser.add_option('-d',"--data",dest='data',help="Z-catted Dataset for T2S Estimation",default=None)
parser.add_option('-e',"--TEs",dest='tes',help="Echo times (in ms) ex: 15,39,103",default='15,39,103')
parser.add_option('-m',"--mask",dest='mask',help="Mask",default=None)
parser.add_option('-p',"--prefix",dest='prefix',help="Prefix",default=None)

def pc(X,mask):
	dat = fmask(X,mask)
	mu = dat.mean(axis=-1)[:,newaxis]
	sig = dat.std(axis=-1)[:,newaxis]

	dat = (dat - mu)/sig

	U,S,V = svd(dat,full_matrices=False)


	return unmask(U,mask), S,V.T


def loglin(catd,mask,tes):
	"""
	t2smap(catd,mask,tes)

	Input:

	catd  has shape (nx,ny,nz,Ne,nt)
	mask  has shape (nx,ny,nz)
	tes   is a 1d numpy array
	"""
	nx,ny,nz,Ne,nt = catd.shape
	N = nx*ny*nz

	echodata = fmask(catd,mask)
	Nm = echodata.shape[0]

	#Do Log Linear fit
	B = echodata.transpose((0,2,1))
	B = np.reshape(echodata,(Nm*nt,Ne)).transpose()
	B = np.log(B)

	nanmask = isnan(B.sum(axis=0))
	B[nanmask] = 0


	x = np.array([np.ones(Ne),-tes]).transpose()
	# X = np.tile(x,(1,nt))
	# X = np.sort(X)[:,::-1].transpose()
	
	# beta,res,rank,sing = np.linalg.lstsq(x,B)
	# beta = polyfit(-tes,B,1)
	beta = dot(pinv(x),B)

	# betas = zeros((2,Nm*nt))
	# betas[:,1-nanmask] = beta

	beta = np.reshape(beta.transpose(),(Nm,nt,2))
	
	r2s = beta[:,:,1]
	s0  = exp(beta[:,:,0])
	
	r2s = unmask(r2s,mask)
	s0  = unmask(s0,mask)

	return exp(r2s), s0

def souheil(catd,mask,tes):

	echodata = fmask(catd,mask)
	Nm = echodata.shape[0]

	dt = unique(diff(tes))

	r = (echodata[:,0,:]*echodata[:,1,:] +  echodata[:,1,:]*echodata[:,2,:])/(echodata[:,0,:]**2+echodata[:,1,:]**2)
	r2s = -log(r)/dt
	
	num = zeros(r2s.shape)
	denom = zeros(r2s.shape)

	for i in range(3):
	
		num += echodata[:,i,:]*exp(-tes[i]*r2s)
		denom+=exp(-2*tes[i]*r2s)

	s0 = num/denom
	s0[np.isnan(s0)] = 0

	r2s = unmask(r2s,mask)
	s0 = unmask(s0,mask)
	return 1/r2s, s0


def numart(catd,mask,tes):
	"""
	t2smap(catd,mask,tes)

	Input:

	catd  has shape (nx,ny,nz,Ne,nt)
	mask  has shape (nx,ny,nz)
	tes   is a 1d numpy array
	"""
	nx,ny,nz,Ne,nt = catd.shape
	N = nx*ny*nz

	# echodata.shape = (Nm,Ne,nt)
	echodata = fmask(catd,mask)
	Nm = echodata.shape[0]

	#Do Log Linear fit
	# B = np.reshape(np.abs(echodata), (Nm,Ne*nt)).transpose()
	# B = np.log(B)
	# x = np.array([np.ones(Ne),-tes])
	# X = np.tile(x,(1,nt))
	# X = np.sort(X)[:,::-1].transpose()

	# beta,res,rank,sing = np.linalg.lstsq(X,B)
	# t2s = 1/beta[1,:].transpose()
	# s0  = np.exp(beta[0,:]).transpose()

	# Use the NUMArt Method from Hagberg 2002 (Posse's group)

	num = np.trapz(echodata,x=tes,axis=1)
	denom = echodata[:,0,:] - echodata[:,-1,:]
	denom[denom<=1] = np.inf
	num[num <= 1]  = 0
	t2s = num / denom

	t2s [t2s <= 0 ] =0 
	t2s[t2s>=400] = 400


	X = np.tile(t2s[:,np.newaxis,:],(1,3,1))
	X [X==0] = np.nan
	X = -tes[np.newaxis,:,np.newaxis]/X 
	X = np.exp(X)

	num   = (X*echodata).sum(axis=1)
	denom = (X**2).sum(axis=1)

	s0 = num/denom
	s0[np.isnan(s0)] = 0

	# out = unmask(t2s.mean(axis=-1),mask)
	out = unmask(t2s,mask)
	out2 = unmask(s0,mask)
	

	return out,out2




(options,args) = parser.parse_args()
if options.prefix:
	pref = '.'+options.prefix
else:
	pref =''

datim  = nib.load(options.data)
head   = datim.get_header()
maskim = nib.load(options.mask)

aff = datim.get_affine()
tes = np.fromstring(options.tes,sep=',',dtype=np.float32)

Ne = tes.shape[0]

dat  = ted.cat2echos(datim.get_data(),Ne)
mask = maskim.get_data()






# r2s,s0 = loglin(dat,mask,tes)

# ted.niwrite(r2s,aff,'r2s.nii')
# ted.niwrite(s0,aff,'s0.nii')

# t2ss,s0s = souheil(dat,mask,tes)
# ted.niwrite(t2ss,aff,'t2s_souheil%s.nii'%pref)
# ted.niwrite(s0s,aff,'s0_souheil%s.nii'%pref)

t2sn,s0n = numart(dat,mask,tes)

nr2 = 1/t2sn[22,35,12,:]
ns0 = s0n[22,35,12,:]
rhobar = ns0.mean()
r2bar = nr2.mean()


edat  = dat[22,35,12,:,:]
mu = edat.mean(axis=-1)

num = mu[0]*mu[1]+mu[1]*mu[2]
denom = mu[0]*mu[0] + mu[1]*mu[1]

r2    = -log(num/denom)/diff(tes).mean()

vec = exp(-r2*tes)
rho   = (mu*vec).sum()/(vec**2).sum()

edat[0,:] = edat[0,:] - mean(edat[0,:]) #rhobar*exp(-r2bar*tes[0]) #mean(edat[0,:]) #rho*exp(-r2*tes[0])
edat[1,:] = edat[1,:] - mean(edat[1,:]) #rhobar*exp(-r2bar*tes[1]) #mean(edat[1,:]) #rho*exp(-r2*tes[1])
edat[2,:] = edat[2,:] - mean(edat[2,:]) #rhobar*exp(-r2bar*tes[2]) #mean(edat[2,:]) #rho*exp(-r2*tes[2])
X = zeros((3,2))

X[:,0] = exp(-r2*tes)*rho*tes.mean()
X[:,1] = -rho*exp(-r2*tes)*tes

delta = dot(pinv(X),edat)
dr2f = delta[1,:]
ds0f = delta[0,:]/rho/tes.mean()


dr2m = (X[:,1][:,newaxis]*edat).sum(axis=0)/(X[:,1]**2).sum()
ds0m = (X[:,0][:,newaxis]*edat).sum(axis=0)/(X[:,0]**2).sum()



st()
ted.niwrite(t2sn,aff,'t2s_numart%s.nii'%pref,head=head)
ted.niwrite(s0n,aff,'s0_numart%s.nii'%pref,head=head)

U,S,V = pc(t2sn,mask)
if not os.path.exists('./t2.pc/'):
	os.mkdir('./t2.pc/')
if not os.path.exists('./s0.pc/'):
	os.mkdir('./s0.pc/')


ted.niwrite(U,aff,'./t2.pc/pc.nii')
savetxt('./t2.pc/pcmix',V)
savetxt('./t2.pc/eigs',S)

U1,S1,V1 = pc(s0n,mask)
ted.niwrite(U1,aff,'./s0.pc/pc.nii')
savetxt('./s0.pc/pcmix',V1)
savetxt('./s0.pc/eigs',S1)

imshow(dot(V1.T,V))
savefig('cross_corr_p.png')
f = genfromtxt('../../../../designs/faces.1D')

o = genfromtxt('../../../../designs/objects.1D')
figure()


# st()
# plot(dot(V.T,f),'r')
# plot(dot(V1.T,f),'--r')
# vG = dot(V[:,:20],dot(V[:,:20].T,f)).std()
# vB = dot(V1[:,:20],dot(V1[:,:20].T,f)).std()
plot(V[:,0])
legend('Good PC 1')
plot(V1[:,0])
legend('Bad PC 1')
savefig('pc1.png')
figure()
plot(V[:,1])
legend('Good PC 2')
plot(V1[:,1])
legend('Bad PC 2')
savefig('pc2.png')
show()
# os.system('mkdir t2.pc ; cd t2.pc; 3dpc -vmean -vnorm -mask ../../eBvrmask.nii.gz -pcsave 10000 ../t2s_numart.nii -overwrite')
# os.system('mkdir s0.pc ; cd s0.pc; 3dpc -vmean -vnorm -mask ../../eBvrmask.nii.gz -pcsave 10000 ../s0_numart.nii -overwrite')

