import os,sys
from pdb import set_trace as st
matfile = sys.argv[1]
numstimts = int(sys.argv[2])

f = open(matfile,'r')
numcols = len(f.readline().strip().split())
f.close()

sh = []
for i in range(1,numcols+1):
    sh.append((" -stim_file %d %s[%d] -stim_label %d matrix%d -stim_base %d ")%(i,matfile,i-1,i,i,i))

print ' -num_stimts %d'%(numcols+numstimts)
print ' '.join(sh)



