#!/usr/bin/env python

import os,sys
import numpy as np
import tedana as meica
import nibabel as nib
from ipdb import set_trace as st


def psc(data):
	mu = data.mean(axis=-1)[:,:,np.newaxis]
	return (data-mu)/mu

catname = sys.argv[1]



cdat = nib.load(catname).get_data()
aff  = nib.load(catname).get_affine()
edata = meica.cat2echos(cdat, 3)
mask  = meica.makemask(edata)

nx,ny,nz,ne,nt = edata.shape

mdat = meica.fmask(edata,mask)
pdat = psc(mdat)
nm = mdat.shape[0]

sigmas = np.zeros((nm,3))

p1 = np.zeros((nm,nt))
p2 = np.zeros((nm,nt))
p3 = np.zeros((nm,nt))

u1 = np.zeros((nm,ne))
u2 = np.zeros((nm,ne))
u3 = np.zeros((nm,ne))


for i in range(nm):
	if (i %100)==0: print float(i)*100/nm
	U, S, V = np.linalg.svd(pdat[i,:,:],full_matrices=False)
	sigmas[i,:] = S
	p1[i,:] = V[0,:]
	p2[i,:] = V[1,:]
	p3[i,:] = V[2,:]
	u1[i,:] = U[:,0]
	u2[i,:] = U[:,1]
	u3[i,:] = U[:,2]

if not os.path.exists('./svd.t'):
	os.mkdir('./svd.t')


out = meica.unmask(sigmas,mask)
meica.niwrite(out,aff,'./svd.t/sigmas.nii')

out = meica.unmask(p1,mask)
meica.niwrite(out,aff,'./svd.t/p1.nii')

out = meica.unmask(p2,mask)
meica.niwrite(out,aff,'./svd.t/p2.nii')

out = meica.unmask(p3,mask)
meica.niwrite(out,aff,'./svd.t/p3.nii')

out = meica.unmask(u1,mask)
meica.niwrite(out,aff,'./svd.t/u1.nii')

out = meica.unmask(u2,mask)
meica.niwrite(out,aff,'./svd.t/u2.nii')

out = meica.unmask(u3,mask)
meica.niwrite(out,aff,'./svd.t/u3.nii')


p1 = np.zeros((nm,nt))
p2 = np.zeros((nm,nt))
p3 = np.zeros((nm,nt))

v1 = np.zeros((nt,ne))
v2 = np.zeros((nt,ne))
v3 = np.zeros((nt,ne))


for i in range(nt):
	if (i %10)==0: print float(i)*100/nt
	U, S, V = np.linalg.svd(pdat[:,:,i],full_matrices=False)
	sigmas[i,:] = S
	p1[:,i] = U[:,0]
	p2[:,i] = U[:,1]
	p3[:,i] = U[:,2]
	v1[i,:] = V[0,:]
	v2[i,:] = V[1,:]
	v3[i,:] = V[2,:]




if not os.path.exists('./svd.s'):
	os.mkdir('./svd.s')


out = meica.unmask(sigmas,mask)
meica.niwrite(out,aff,'./svd.s/sigmas.nii')

out = meica.unmask(p1,mask)
meica.niwrite(out,aff,'./svd.s/p1.nii')

out = meica.unmask(p2,mask)
meica.niwrite(out,aff,'./svd.s/p2.nii')

out = meica.unmask(p3,mask)
meica.niwrite(out,aff,'./svd.s/p3.nii')

out = meica.unmask(u1,mask)
meica.niwrite(out,aff,'./svd.s/u1.nii')

out = meica.unmask(u2,mask)
meica.niwrite(out,aff,'./svd.s/u2.nii')

out = meica.unmask(u3,mask)
meica.niwrite(out,aff,'./svd.s/u3.nii')

