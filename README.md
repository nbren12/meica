
# Dependencies

1. Python 2.7 (Python 2.6 is possible but annoying)
2. numpy/scipy
3. nibabel
4. afni (for preprocessing)
5. fsl (for `melodic`)

# Installation

If you don't have numpy/scipy installed, I would recommend using [Enthought Python Distribution](http://enthought.com/products/epd.php).

1. Install Python and other dependencies
2. Add the `meica` directory to your PATH and PYTHONPATH environmental variables
3. Unzip mdp33.zip 


# Important Files

- `meica2.py` : a master script that performs preprocessing and calls the meat and potatoes script `tedana.py`
- `fitmod.py` : a script that performs fits for \delta R_2^* and \delta \rho

- `tedana2.py` : the meat and potatoes of the operation. Performs the TE dependence calculations.


# Usage

fMRI data is called: 		rest_e1.nii.gz, rest_e2.nii.gz, rest_e3.nii.gz, etc. 
anatomical is:		mprage.nii.gz

meica.py and tedana.py have a number of options which you can view using the -h flag. 

Here's an example use:

    meica2.py -d 'rest1.e0[2,1,3].nii.gz' -e 17,40,63 -b 15.0 -f 5mm -a mprage.nii --OVERWRITE -o --keep-int

This means:

    -e are the echo times, -d are the data (note format), -a is a "raw" mprage with a skull 
    -o means data are oblique (good to keep on)  
    --TR 2.5 means a TR of 2.5 seconds 
    -b 15 means drop first 15 seconds of data for equilibration
    -f 6mm means blur _to_ a smoothness of 6mm (using 3dBlurToFWHM, some blur (>3mm) required for spatial ICA)


## `fitmod.py`

See `-h` flag for usage. Here is an example, assuming you have preprocessed the data using meica2.py

	# Make a fitmod directory and do some more preprocessing
	mkdir fitmod && cd fitmod
	3dZcat -prefix ./zcat_vr.nii.gz ../e1_vr.nii.gz ../e2_vr.nii.gz ../e3_vr.nii.gz
	3dTstat -prefix zcat_mean.nii.gz zcat_vr.nii.gz
	3dBandpass -prefix _tmp_zcat_bp.nii.gz  -input zcat_vr.nii.gz 0.01 99
	3dcalc -a  _tmp_zcat_bp.nii.gz -b zcat_mean.nii.gz -expr 'a+b' -prefix zcat_bp.nii.gz

	# Do TE depedence analysis
	fitmod.py -e 15,39,63 -m ../eBvrmask.nii.gz -d zcat_bp.nii.gz


## Output

- `./meica.rest1.e0213/` : contains preprocessing
- `./meica.rest1.e0213/TED` : contains TED stuff
- `./meica.rest1.e0213/TED/accepted.txt` : 0 indexed list of accepted components
- `./meica.rest1.e0213/TED/rejected.txt` : 0 indexed list of rejected components
- `./meica.rest1.e0213/TED/comp_table.txt` : text file containing kappa/rho values


